<div class="row">
  <!-- Left Side -->
  <div class="col-md-5">
    <div class="newcontainer">
      <div class="p-5">
        <!-- Logo -->
        <div class="text-left">
           <img src="<?php echo base_url('assets/dist/img/logo.png') ?>">
        </div>
        <!-- End Logo -->
        <div class="text-left">
           <p class="text-login">Silahkan registrasi untuk mengakses membuat akun member</p>
        </div>
        <!-- Form Login -->
        <form class="user mt-7">
          <div class="form-group">
            <label for="exampleFormControlInput1">Nama Lengkap</label>
            <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Nama Lengkap">
          </div>
          <div class="form-group">
            <label for="exampleFormControlInput1">Username</label>
            <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Username">
          </div>
          <div class="form-group">
            <label for="exampleFormControlInput1">Password</label>
            <input type="password" class="form-control" id="exampleFormControlInput1" placeholder="Password">
          </div>
          <div class="form-group">
            <label for="exampleFormControlInput1">No HP</label>
            <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="No HP">
          </div>
          <input type="submit" name="" class="btn btn-primary btn-block mt-4" value="Daftar">
        </form>
        <!-- End Form Login -->
    </div>
    </div>
  </div>
  <!-- End Left Side -->
  <!-- Right Side -->
  <div class="col-md-6">
    <img src="<?php echo base_url('assets/dist/img/background.png') ?>">
  </div>
  <!-- End Right Side -->
</div>
