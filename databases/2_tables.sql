/* TABLES > USERS */
CREATE TABLE users (
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(255),
    password VARCHAR(255),
    full_name VARCHAR(255),
    phone_number VARCHAR(15),
    address VARCHAR(255) NULL,
    face TEXT NULL,
    created_at VARCHAR(20) NULL,
    updated_at VARCHAR(20) NULL
);

/* TABLES > USERS > UTILITY */
CREATE TABLE users_utility (
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    user_type TINYINT(1) DEFAULT 0,
    balance BIGINT(20) DEFAULT 0,
    transaction BIGINT(20) DEFAULT 0,
    stuff BIGINT(20) DEFAULT 0,
    supplier BIGINT(20) DEFAULT 0,
    user_id INT(11),
    INDEX user_id_utility (user_id),
    FOREIGN KEY (user_id)
        REFERENCES users(id)
        ON DELETE CASCADE
);